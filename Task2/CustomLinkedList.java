public class CustomLinkedList {

    private Node head;
    private int size;

    public CustomLinkedList() {
        Node node = new Node(0, null);
        this.head = node;
    }

    public void add(int value){
        Node tail = getNodeByIndex(size-1);
        Node node = new Node(value, null);
        tail.next = node;
        size++;
    }

    public void add(int value, int index){
        checkIndex(index);
        Node previous = getNodeByIndex(index-1);
        Node next = previous.next;
        Node node = new Node(value, next);
        previous.next = node;
        size++;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size){
            throw new IndexOutOfBoundsException("Size is: " + size + ", index: " + index);
        }
    }

    public void removeByValue(int value){
        Node current = head.next;
        Node previous = head;
        while(current != null){
            if(current.value == value){
                Node next = previous.next.next;
                previous.next = next;
                size--;
            }
            previous = current;
            current = current.next;
        }
    }

    public void removeByIndex(int index){
        checkIndex(index);
        Node previous = getNodeByIndex(index - 1);
        Node next = previous.next.next;
        previous.next = next;
        size--;
    }

    public void replace(int value, int index){
        Node current = getNodeByIndex(index);
        current.value = value;
    }

    public int get(int index){
        checkIndex(index);
        Node current = getNodeByIndex(index);
        return current.value;
    }

    private Node getNodeByIndex(int index) {
        if(index == -1){
            return head;
        }

        Node current = head.next;
        for(int i = 0; i < index; i++){
            current = current.next;
        }
        return current;
    }

    public int getSize(){
        return size;
    }

    private class Node{
        private int value;
        private Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }

        public Node getNext() {
            return next;
        }
    }
}
