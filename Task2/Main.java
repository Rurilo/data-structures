public class Main {
    public static void main(String[] args) {

        CustomLinkedList list = new CustomLinkedList();
        list.add(3);
        list.add(9);
        list.add(4);
        list.add(10);
        list.add(50);
        list.add(43);

        print(list);

        list.add(500,2);
        print(list);
        list.replace(900,2);
        print(list);
        list.removeByValue(900);
        print(list);
        list.removeByIndex(4);
        print(list);
    }

    private static void print(CustomLinkedList list) {
        for(int i = 0; i < list.getSize(); i++){
            System.out.println(list.get(i));
        }
        System.out.println();
    }
}