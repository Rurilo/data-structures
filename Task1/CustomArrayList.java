import java.util.Arrays;

public class CustomArrayList {

    private static final int DEFAULT_SIZE = 10;

    private int[] elements;

    private int currentIndex;

    public CustomArrayList() {

        elements = new int[DEFAULT_SIZE];
    }

    public void add (int element){
        if(currentIndex >= elements.length){
            doubleArraySize();
        }
        elements[currentIndex++] = element;
    }

    public void add(int element, int index){

        if(index >= elements.length){
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }

        if(currentIndex >= elements.length){
            doubleArraySize();
        }

        int j = 0;
        int newSize = elements.length + 1;
        int[] newElements = new int[newSize];
        for(int i = 0; i < newSize - 1; i++){
            if(i == index){
                newElements[j] = 0;
                j++;
            }
            newElements[j] = elements[i];
            j++;
        }

        newElements[index] = element;
        elements = newElements;
    }

    private void doubleArraySize(){
        int newSize = elements.length * 2;
        int[] newElements = new int[newSize];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;
    }

    public void removeByIndex(int index){
        if(index >= elements.length){
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }

        int size = elements.length-1;
        int[] newElements = new int[size];
        int j = 0;
        for(int i = 0; i < size+1; i++){
            if(i == index){
                continue;
            }
            newElements[j] = elements[i];
            j++;
        }

        elements = newElements;
    }

    public void removeByValue(int value){

        int size = elements.length-1;
        int[] newElements = new int[size];
        boolean isExist = false;

        for(int i = 0; i < elements.length; i++){
            if(elements[i] == value){
                isExist = true;
            }
        }

        if(isExist){
            int j = 0;
            for(int i = 0; i < size+1; i++){
                if(elements[i] == value){
                    continue;
                }
                newElements[j] = elements[i];
                j++;
            }
            elements = newElements;
        }else{
            System.out.println("There is not such element");
        }
    }

    public void replace(int element, int index){
        if(index >= elements.length){
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }

        for(int i = 0; i < elements.length; i++){
            if(i == index){
                elements[i] = element;
            }
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(elements);
    }
}
