public class Main {
    public static void main(String[] args) {

        CustomArrayList customArrayList = new CustomArrayList();

        for (int i = 0; i < 10; i++){
            customArrayList.add(i);
        }

        System.out.println(customArrayList);

        customArrayList.add(11);
        System.out.println(customArrayList);

        customArrayList.add(500,3);
        System.out.println(customArrayList);

        customArrayList.removeByIndex(3);
        System.out.println(customArrayList);

        customArrayList.add(666,7);
        System.out.println(customArrayList);

        customArrayList.removeByValue(666);
        System.out.println(customArrayList);

        customArrayList.replace(474747, 5);
        System.out.println(customArrayList);




    }
}